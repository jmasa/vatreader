package eu.jmasa.vatreader;

/** Immutable class keeps track of:
 *    country name
 *    standard VAT rate
 *    effective_from 
 *   
 *   Date effective_from is held as String and expected in mandatory format "YYYY-MM-DD",
 *   comparison of dates is based on this specific format.
 *   Such handling is pretty easy and straightforward. Perhaps good for demo. 
 *    
 * @author Jim
 *
 */
public class EUCountryVAT implements Comparable<EUCountryVAT> {
    String name;
    String effective_from;
    double standard;
	
    
    EUCountryVAT(String name, String effective_from, double standard) {
    	 this.name           = name;
    	 this.effective_from = effective_from;
    	 this.standard       = standard;	
    };

   
	public int compareTo(EUCountryVAT o) {
		int ret  = name.compareTo(o.name);
		
		if (ret == 0) ret = effective_from.compareTo(o.effective_from);
		
		return ret;
	}

	/**
	 * 
	 * @param date - not null date in format "YYYY-MM_DD".
	 * @return true if date is older or equal this.effective_date 
	 */
    boolean isValidAfter(String date) {
    	return (effective_from.compareTo(date) >= 0);
    }
	
    static boolean checkDate(String date) {
    	return (date != null) && date.matches("\\d\\d\\d\\d-\\d\\d-\\d\\d");
    }

    static boolean checkRate(Double rate) {
    	return (rate != null) && (rate >= 0.0); //  && (rate <= 100); is there any tax limit????
    }
    
    public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEffective_from() {
		return effective_from;
	}


	public void setEffective_from(String effective_from) {
		this.effective_from = effective_from;
	}


	public double getStandard() {
		return standard;
	}


	public void setStandard(double standard) {
		this.standard = standard;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((effective_from == null) ? 0 : effective_from.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		long temp;
		temp = Double.doubleToLongBits(standard);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		
		EUCountryVAT other = (EUCountryVAT) obj;
		
		if (effective_from == null) {
			if (other.effective_from != null) return false;
		} else if (!effective_from.equals(other.effective_from)) return false;
		if (name == null) {
			if (other.name != null) return false;
		} else if (!name.equals(other.name)) return false;
		if (Double.doubleToLongBits(standard) != Double.doubleToLongBits(other.standard)) return false;
		
		return true;
	}


	@Override
	public String toString() {
		return "EUCountryVAT [name=" + name + ", effective_from=" + effective_from + ", standard=" + standard + "]";
    }

}
