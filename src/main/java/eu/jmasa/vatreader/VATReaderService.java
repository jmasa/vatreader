package eu.jmasa.vatreader;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 * Task: Implement an application in Java capable of printing out three EU countries with the lowest
 * and three EU countries with the highest standard VAT rate as of today within the EU.
 * Input: http://jsonvat.com/
 * 
 * 
 * This class provides static methods, which read and process input JSON data in a specific format.
 * The code tries to do "as much as possible" meaning, that when individual record data is not correct
 * or consistent such a record is ignored and other records are processed. 
 * 
 * @author Jim
 *
 */
public class VATReaderService {

	/**
	 * 
	 * @param urlStr - well formed url string expected
	 * @return open input stream
	 * 
	 * @throws IOException
	 */
    public static InputStream openInputStreamFromURL(String urlStr) throws IOException {
        URL url = new URL(urlStr);
        
        HttpURLConnection connection =  (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        return connection.getInputStream();
    }

    /** Read stream and store its data as string.
     *  The stream gets closed at the end (even in case of error).
     * 
     *  TODO: check size and implement limit to avoid RAM overflow.
     *  
     * @param inpStream - the stream to read from 
     * @return String data read
     * 
     * @throws IOException (stream gets closed)
     */
    public static String downloadToString(InputStream inpStream) throws IOException {       
    	Scanner scanner = null;
    	String ret = null;
    	
    	try {
    		scanner = new Scanner(inpStream, StandardCharsets.UTF_8.name());
    	    ret = scanner.useDelimiter("\\A").next();
    	}
    	finally {
    		inpStream.close();
        	if (scanner != null) scanner.close();
    	}
    	
    	return ret;
    }

	
    /** Input data parser and selector, errors and inconsistencies are reported to STDERR,
     *  invalid records are ignored.
     * 
     * @param vatDataJSON - data read from input stream, valid JSON 
     * @return a list of records sorted by VAT standard rate, list contains only relevant
     *         records "valid as of today"
     * 
     * @throws IOException
     */
    public static List<EUCountryVAT> parseInput(String vatDataJSON) throws IOException {
		
		final String today = getTodayAsString();   // today as YYYY-MM-DD
		
		List<EUCountryVAT> countryList = new ArrayList<EUCountryVAT>();  // an empty output list 
		
		JsonParamParser parser = new JsonParamParser(vatDataJSON);
		
		int countryCount = parser.getNodeSizeEx("rates");               // size of "rates" array 
		
		countryLoop:
		for (int i = 0; i < countryCount; i++) {                       // for every country in the "rates" array

			String name = parser.getParameterEx(null, "rates", i, "name");   // store name of the country
			
			if (name == null) {
				System.err.println("Input data: attribute name is missing"); // name not found - ignore country
				continue;
			}
			
			int periodsCount =  parser.getNodeSizeEx("rates", i, "periods"); // VAT rate periods are stored as an array
			
			EUCountryVAT countryVAT = null;
			
			// find rate valid for current date
			//
			for (int j = 0; j < periodsCount; j++) { 
				String date =  parser.getParameterEx(null, "rates", i, "periods", j, "effective_from").trim();
				Double rate =  parser.getDoubleParameterEx(null, "rates", i, "periods", j, "rates", "standard");

				
				if (EUCountryVAT.checkDate(date) && EUCountryVAT.checkRate(rate)) {  // check input format and standard VAT % rate 
					
					EUCountryVAT euVAT = new EUCountryVAT(name, date, rate);
					
					if (countryVAT == null) {   // the very first VAT record for a country 
						if (!euVAT.isValidAfter(today))  // not in future
                            countryVAT = euVAT;    						
					}
					else {
						// date as of today and not in future
						if (euVAT.isValidAfter(countryVAT.getEffective_from()) && (!euVAT.isValidAfter(today))) {
							countryVAT = euVAT;
						}
					}  					
				}
				else {                                       // input data format inconsistency - ignore the country
					System.err.println("Country ignored: " + name);
					System.err.println("Input data: wrong or missing date or rate: " + date + "; " + rate);
					continue countryLoop;
				}
			}
			
			if (countryVAT != null) {           // at least one VAT record found
				countryList.add(countryVAT);
			}
			else {
				System.err.println("Input data:  country ignored, no standard rate data for: " + name);
			} 			
		}
		
		// Sort list of country VAT records by ad-hoc "VAT standard rate" comparator 
		//
		Collections.sort(countryList, new Comparator<EUCountryVAT>() {
			public int compare(EUCountryVAT o1, EUCountryVAT o2) {
				return Double.compare(o1.getStandard(), o2.getStandard());
			}}
		);
		
		return countryList;
	}

    /** Print three min. and max. VAT countries from the list. 
     *  Country names can "overlap" in case that input stream consists of 
     *  less than six valid country records.
     *  
     *  Warning is issued if VAT ranking provides more candidates to print. 
     *  
     * @param countryList
     */
    public static void printThreeMinMax(List<EUCountryVAT> countryList) {	
		int idx = countryList.size() - 1;
		
		if ((countryList == null) || (countryList.size() < 3)) {
			System.err.println("Not enough data.");
			return;
		}
		
		System.out.println("Max standard VAT rate:");
		for (int i = 0; i < 3; i++) {
			System.out.println(countryList.get(idx--).toString());
		}
		
		if ((idx >= 0) && (countryList.get(idx).getStandard() == countryList.get(idx+1).getStandard())) {
			System.out.println("Three countries listed, but there are more with standard VAT " + countryList.get(idx).getStandard() + "%");
		}

		
		idx = 0;
		System.out.println("\nMin standard VAT rate:");
		for (int i = 0; i < 3; i++) {
			System.out.println(countryList.get(idx++).toString());
		}

		if ((idx < countryList.size()-1) && (countryList.get(idx).getStandard() == countryList.get(idx-1).getStandard())) {
			System.out.println("Three countries listed, but there are more with standard VAT " + countryList.get(idx-1).getStandard() + "%");
		}
    }

    /** For testing ...
     * 
     * @param fileName
     * @return
     * @throws IOException
     */
    static String readFileString(String fileName) throws IOException {
     	 return new String(Files.readAllBytes(Paths.get(fileName)), StandardCharsets.UTF_8);
     }

    private static String getTodayAsString() {
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    	return df.format(new Date());
    }

    
    public static void main(String[] argv) {
    	String vatURL = "http://jsonvat.com/";
                        
    	try {
    		
    		String vatDataJSON = downloadToString(openInputStreamFromURL(vatURL));
    		
			printThreeMinMax(parseInput(vatDataJSON));
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
    }
}
