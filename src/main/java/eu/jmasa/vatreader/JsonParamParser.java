package eu.jmasa.vatreader;

import java.io.IOException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.NullNode;

/** 
 * 
 * @author Jim
 *
 */
public class JsonParamParser {
    protected JsonNode rootNode;

    /** Create instance initialized with valid JSON String.
     * 
     * @param jsonAsString valid JSON data as String,
     *        when null or "" it uses {}
     * @throws IOException - jackson.readTree() 
     */
    public JsonParamParser(String jsonAsString) throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        if (jsonAsString == null || jsonAsString.length() == 0) jsonAsString = "{}";

        rootNode = mapper.readTree(jsonAsString);
    }

    
    /**
     * An extended variant. I allows traversal through JSON tree hierarchy.
     *
     * @param defaultValue - is returned in case of error or attribute/array index  is not found 
     * @param parameterNames - list of String or Integer objects interpreted as attribute names or array indices constituting "path" trough JSON tree.
     *                         The path starts with highest attribute.
     * @return JSON attribute value as string. If parameterNames path ends with object (not with leaf) it returns an empty String (see JsonNode.asText()).    
     */
    public String getParameterEx(String defaultValue, Object... parameterNames) {
        JsonNode node = getLeafParameterNodeEx(parameterNames);

        if (node != null) defaultValue = node.asText();

        return defaultValue;
    }

    
    public Double getDoubleParameterEx(Double defaultValue, Object... parameterNames) {
        JsonNode node = getLeafParameterNodeEx(parameterNames);

        return nodeToDouble(defaultValue, node);
    }

    

    /** Find size of JsonNode (use for arrays).
     *
     * @param parameterNames list of String or Integer objects interpreted as attribute names or array indices constituting "path" trough JSON tree
     * @return JsonNode.size() or -1 if node is not found 
     */
    public int getNodeSizeEx(Object... parameterNames) {
        
    	if (parameterNames.length == 0) return rootNode.size();
    	
    	JsonNode node = getLeafParameterNodeEx(parameterNames);

        if (node != null) return node.size();

        return -1;
    }

    /** Convert JsonNode to Double
     * 
     * @param defaultValue - is returned in case of error or attribute/array index  is not found
     * @param node - node to convert
     * @return converted double or null (if it is JSON attribute value); default in case of parsing error 
     */
    private Double nodeToDouble(Double defaultValue, JsonNode node) {
        double paramValue;
        
        if (node == null) return defaultValue;      // parametr pro node nebyl nalezen
        if (node instanceof NullNode) return null;  // hodnota je "null"
        
        paramValue = node.asDouble(Double.MIN_VALUE);
        
        if ((paramValue == Double.MIN_VALUE) && (node.asDouble() == 0.0)) return defaultValue; // chyba parsovani

        return paramValue;
    }

    /** Traverse along the path.
     * 
     * @param parameterNames
     * @return value or last attribute or null if parameterNames array is empty. 
     */
    protected JsonNode getLeafParameterNodeEx(Object... parameterNames) {
        JsonNode node = rootNode;
        int cnt = parameterNames.length;
        int i = 0;

        if (parameterNames.length == 0) return null;

        do {
            if ((parameterNames[i] instanceof String) && (! node.isArray())) {
                node = node.get((String)parameterNames[i]);
            }
            else if ((parameterNames[i] instanceof Integer) && (node.isArray())) {
                ArrayNode arr = (ArrayNode) node;
                int idx = (Integer)parameterNames[i];
                node = arr.get(idx);
            }
            else {
                throw new IllegalArgumentException("Only String (as name) and Integer (as index) parameters supported; passed " + parameterNames[i].getClass().getName());
            }
        } while (++i < cnt && node != null);

        return node;
    }
}

    